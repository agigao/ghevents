from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils, OffsetRange, TopicAndPartition
import json

PERIOD = 15
BROKERS = 'localhost:9092'
TOPIC = 'ghevents'

conf = SparkConf().set('spark.default.parallelism', 1)
sc = SparkContext(appName="github-events", conf=conf)
ssc = StreamingContext(sc, PERIOD)

dstream = KafkaUtils.createDirectStream(
    ssc,
    [TOPIC],
    {
        'metadata.broker.list': BROKERS,
        'group.id': '0',
    }
)

try:
    object_stream = dstream.map(lambda x: json.loads(x[1]))
    object_stream.pprint()
    dstream.count().pprint()
    # object_stream.saveAsTextFiles("hdfs://localhost:9000/")
except:
    pass

ssc.start()
ssc.awaitTermination()
